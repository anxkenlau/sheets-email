import pygsheets
import subprocess
import time
import calendar
import email_notify
import mysql.connector
from datetime import datetime
import traceback

#pip install gdata
#pip install gspread
#pip install oauth2client==1.5.2
#pip install PyOpenSSL

#pip install --upgrade google-api-python-client
#pip install pygsheets
#pip install html2text
def iter_row(cursor, size=10):
    while True:
        rows = cursor.fetchmany(size)
        if not rows:
            break
        for row in rows:
            yield row

class SimpleCRUD:
    def __init__(self):

        gc = pygsheets.authorize(service_file='/home/joggerjoel/AnypointStudio/sheets-email/gsheets-gmail/src/main/resources/API-Project-97866-0bb6aace17c7.json')

        sh = gc.open('Stage Trader')

        lastTime = 0

        while True:
            currentTime = calendar.timegm(time.gmtime())

            #print currentTime - lastTime

            if (currentTime - lastTime > 60*5):

                bashCommand =  "curl -A \"User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\" -s https://www.personal.hsbc.com.hk/1/2/hk/investments/mkt-info/fcy|grep -A 15 \"US Dollar\"| grep hsbcAlign06 | html2text | awk -F '|' '{print $1, $2, $5'}"
                output = subprocess.check_output(['bash', '-c', bashCommand])
                usdFX = output.split(' ')

                fxTime = str(usdFX[8:-2]).replace(']','').replace('[','').replace('\'','').replace(',','').split(' ')

                #print (' '.join(fxTime) . " " . usdFX[0] . " " . usdFX[3])

                try:
                    cover = sh.worksheet('index', 0)
                    cover.update_cell("D2", usdFX[0])
                    cover.update_cell("E2", usdFX[3])
                    cover.update_cell("F2", ' '.join(fxTime))
                except Exception, e:
                    print e
                    print traceback.format_exc()

                lastTime = calendar.timegm(time.gmtime())


            try:
                cell_matrix = cover.get_all_values(returnas='matrix')
                numcols = len(cell_matrix[0]) # 2 columns in your example
                numrows = len(cell_matrix)    # 3 rows in your example


            except Exception, e:
                print worksheet
                print e
                print traceback.format_exc()

            totalExposure = cell_matrix[26][2]
            riskCheck4 = cell_matrix[26][7]
            riskCheck3 = cell_matrix[26][6]
            riskCheck2 = cell_matrix[26][5]
            riskCheck1 = cell_matrix[26][4]
            riskPercent = cell_matrix[26][3]

            emailStatus4 = cell_matrix[27][7]
            emailStatus3 = cell_matrix[27][6]
            emailStatus2 = cell_matrix[27][5]
            emailStatus1 = cell_matrix[27][4]

            emailRisk = False

            if (riskCheck4 == 'TRUE') and (emailStatus4 == 'CLEAR'):
                cover.update_cell("H28", 'TRIGGERED')
                emailRisk = True
            elif (riskCheck3 == 'TRUE') and (emailStatus3 == 'CLEAR'):
                cover.update_cell("G28", 'TRIGGERED')
                emailRisk = True
            elif (riskCheck2 == 'TRUE') and (emailStatus2 == 'CLEAR'):
                cover.update_cell("F28", 'TRIGGERED')
                emailRisk = True

            elif (riskCheck1 == 'TRUE') and (emailStatus1 == 'CLEAR'):
                cover.update_cell("E28", 'TRIGGERED')
                emailRisk = True

            if (emailRisk):
                try:
                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                    elist = [{
                        "email": "oslops@octagonfinancial.ltd",
                        "name": "Simon",
                        "subject": "Risk Breach",
                        "data" : { 
                            "riskPercent": str(riskPercent),
                            "totalExposure": str(totalExposure),
                        }
                    }]         
                    en.mailbulkInternal(elist, "trade_risk")

                except Exception, e:
                    print e 
                    print traceback.format_exc()

            if (riskCheck4 == 'FALSE') and (riskCheck3 == 'TRUE') and (emailStatus4 == 'TRIGGERED'):
                cover.update_cell("H28", 'CLEAR')
            elif (riskCheck3 == 'FALSE') and (riskCheck2 == 'TRUE') and (emailStatus3 == 'TRIGGERED'):
                cover.update_cell("G28", 'CLEAR')
            elif (riskCheck2 == 'FALSE') and (riskCheck1 == 'TRUE') and (emailStatus2 == 'TRIGGERED'):
                cover.update_cell("F28", 'CLEAR')
            elif (riskCheck1 == 'FALSE') and (emailStatus1 == 'TRIGGERED'):
                cover.update_cell("E28", 'CLEAR')


            for x in range(1, 7):

                worksheet = sh.worksheet('index', x)
                #print "worksheet"
    #            print worksheet
    #            print worksheet.split('\'')
                #print worksheet.title
                # With coords

                numcols = 0

                try:
                    cell_matrix = worksheet.get_all_values(returnas='matrix')
                    numcols = len(cell_matrix[0]) # 2 columns in your example
                    numrows = len(cell_matrix)    # 3 rows in your example

                except Exception, e:
                    print worksheet
                    print e
                    print traceback.format_exc()

                if (numcols == 24):
                    googleCell=chr(ord('A')+9)

                    for numrow in range(6, numrows):
                        operations = cell_matrix[numrow][9]
                        fiatStatus = cell_matrix[numrow][10]
                        cryptoStatus = cell_matrix[numrow][11]

                        #if (operations == "Instructions" and fiatStatus == "Pending" and cryptoStatus == "Pending"):
                        #print cell_matrix[numrow][3]+"|"+operations+"|"+fiatStatus+"|"+cryptoStatus
                        if ((operations == "Instructions" or operations == "i" or operations == "Verify") and fiatStatus == "Pending" and cryptoStatus == "Pending"):
                            uuid = cell_matrix[numrow][0]
                            print "uuid: "+uuid

                            side = cell_matrix[numrow][4].strip()
                            print "side: "+side

                            code = cell_matrix[numrow][2]
                            print "code: "+code

                            transaction = cell_matrix[numrow][3]
                            print "transaction: "+transaction

                            trader_email = cell_matrix[numrow][18]
                            print "trader_email: "+trader_email


                            price = 0.0
                            quantity = 0.0

                            if (side == "Buy"):
                                sign = "+"
                                action = "SOLD"

                                try:
                                    print cell_matrix[numrow][5].replace('\$','').replace(',','')
                                    coinQuantity = float(cell_matrix[numrow][5].replace('\$','').replace(',',''))
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                    try:
                                        worksheet.update_cell(googleCell+str(numrow+1), "Rejected")
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()

                                    try:
                                        en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                        elist = [{
                                            "email": trader_email,
                                            "name": "Simon",
                                            "subject": str(transaction),
                                            "data" : { 
                                                "uuid": str(uuid),
                                                "coin": str(worksheet.title),
                                                "quantity": str(cell_matrix[numrow][5].replace('\$','').replace(',','')),
                                                "action": str(action),
                                            }
                                        }]         
                                        en.mailbulkInternal(elist, "trade_error")
                           
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()
                                    continue

                                payment = cell_matrix[numrow][13]


                                try:
                                    sellCoin = 0.0
                                    buyCoin = float(cell_matrix[numrow][5].replace('$','').replace(',',''))
                                    print "buyCoin: " + str(buyCoin)
                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                    try:
                                        worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                    except Exception, e:
                                        print e
                                        print traceback.format_exc()

                                    try:
                                        en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                        elist = [{
                                            "email": trader_email,
                                            "name": "Simon",
                                            "subject": str(transaction),
                                            "data" : { 
                                                "uuid": str(uuid),
                                                "coin": str(worksheet.title),
                                                "quantity": "{:20,.8f}".format(coinQuantity),
                                                "action": str(action),
                                                "buyCoin": str(cell_matrix[numrow][5].replace('$','').replace(',','')),
                                            }
                                        }]         
                                        en.mailbulkInternal(elist, "trade_error")
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()

                                    continue

                            elif (side == "Sell"):
                                sign = "-"
                                action = "BOUGHT"

                                try:
                                    print cell_matrix[numrow][6].replace('\$','').replace(',','')
                                    coinQuantity = float(cell_matrix[numrow][6].replace('$','').replace(',',''))
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()
                                    try:
                                        worksheet.update_cell(googleCell+str(numrow+1), "Rejected")
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()

                                    try:
                                        en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                        elist = [{
                                            "email": trader_email,
                                            "name": "Simon",
                                            "subject": str(transaction),
                                            "data" : { 
                                                "uuid": str(uuid),
                                                "coin": str(worksheet.title),
                                                "quantity": str(cell_matrix[numrow][6].replace('\$','').replace(',','')),
                                                "action": str(action),
                                            }
                                        }]         
                                        en.mailbulkInternal(elist, "trade_error")
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()
                                    continue
    
                                payment = cell_matrix[numrow][12]

                                try:
                                    buyCoin = 0.0
                                    sellCoin = cell_matrix[numrow][6].replace('$','').replace(',','')
                                    print "sellCoin: "+ str(sellCoin)
                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                    try:
                                        worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                    except Exception, e:
                                        print e
                                        print traceback.format_exc()

                                    try:
                                        en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                        elist = [{
                                            "email": trader_email,
                                            "name": "Simon",
                                            "subject": str(transaction),
                                            "data" : { 
                                                "uuid": str(uuid),
                                                "coin": str(worksheet.title),
                                                "quantity": "{:20,.8f}".format(coinQuantity),
                                                "action": str(action),
                                                "sellCoin": str(cell_matrix[numrow][6].replace('$','').replace(',','')),
                                            }
                                        }]         
                                        en.mailbulkInternal(elist, "trade_error")
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()

                                    continue


                            else:
                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")
                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "action": str("INCORRECT"),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()


                            try:
                                costUSD = float(cell_matrix[numrow][7].replace('$','').replace(',',''))
                                print "coinUSD:" + str(costUSD)
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "cost "+str(cell_matrix[numrow][7].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            try:
                                threshold = float(cell_matrix[numrow][8].replace('$','').replace(',',''))
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "threshold "+str(cell_matrix[numrow][8].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            try:
                                recieveUSD = float(cell_matrix[numrow][12].replace('$','').replace(',',''))
                                print "receiveUSD: "+str(recieveUSD)
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "recieved "+str(cell_matrix[numrow][12].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            try:
                                sentUSD = float(cell_matrix[numrow][13].replace('$','').replace(',',''))
                                print "sentUSD: "+str(sentUSD)
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()


                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "sent "+str(cell_matrix[numrow][13].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            try:
                                fee_percent = float(cell_matrix[numrow][14].replace('%',''))
                                print "fee_percent: " +str(fee_percent)
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()


                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "fee_percent "+str(cell_matrix[numrow][14].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            try:
                                fee_profit = float(cell_matrix[numrow][15].replace('$','').replace(',',''))
                                print "fee_profit: "+str(fee_profit)
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()


                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "fee_profit "+str(cell_matrix[numrow][15].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            try:
                                payment = float(cell_matrix[numrow][16].replace('$','').replace(',',''))
                                print "payment: "+str(payment)
                            except Exception, e:
                                print e
                                print traceback.format_exc()

                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()


                                try:
                                    en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "action": str(action),
                                            "error": "payment "+str(cell_matrix[numrow][16].replace('$','').replace(',','')),
                                        }
                                    }]         
                                    en.mailbulkInternal(elist, "trade_error")
                                except Exception, e:
                                    print e 
                                    print traceback.format_exc()

                                continue

                            finder_fee = cell_matrix[numrow][17]
                            print "finder_fee: "+str(finder_fee)

                            fiat = cell_matrix[numrow][19]
                            print "fiat: "+fiat

                            action = cell_matrix[numrow][20]
                            print "action: "+action

                            client_email = cell_matrix[numrow][21]
                            print "client_email: "+client_email


                            bookDate = cell_matrix[numrow][1]
                            print "bookDate: "+bookDate



                            print googleCell+str(numrow+1)

                            #print (trader_email)
                            #print (client_email)
                            if trader_email and  client_email:
                                if float(threshold) > 0.05 or float(threshold) < -0.05:
                                    try:
                                        worksheet.update_cell(googleCell+str(numrow+1), "Rejected")
                                    except Exception, e:
                                        print e
                                        print traceback.format_exc()     

                                    try:
                                        en = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                        elist = [{
                                            "email": trader_email,
                                            "client_email": client_email,
                                            "name": "Simon",
                                            "subject": str(transaction),
                                            "data" : { 
                                                "uuid": str(uuid),
                                                "coin": str(worksheet.title),
                                                "quantity": "{:20,.8f}".format(coinQuantity),
                                                "action": str(action),
                                                "error": "threshold 5% "+str(threshold),
                                            }
                                        }]         
                                        en.mailbulkInternal(elist, "trade_error")
                                    except Exception, e:
                                        print e 
                                        print traceback.format_exc()

                                    continue

                                try:
                                    e = email_notify.EmailNotification("smtp-relay.gmail.com", 587, "Octagon Strategy", "_system-smtp-relay@octagonstrategy.ltd", "_system-smtp-relay@octagonstrategy.ltd", "")
                                    elist = [{
                                        "email": trader_email,
                                        "client_email": client_email,
                                        "name": "Simon",
                                        "subject": str(transaction),
                                        "data" : { 
                                            "uuid": str(uuid),
                                            "coin": str(worksheet.title),
                                            "quantity": "{:20,.8f}".format(coinQuantity),
                                            "price": "{:20,.8f}".format(costUSD),
                                            "fiat": str(fiat),
                                            "action": str(action),
                                            "commission": "{:20,.8f}".format(fee_profit),
                                            "commissionpct": "{:20,.4f}".format(fee_percent),
                                            "total": "{:20,.4f}".format(payment),
                                        }
                                    }]
                                except Exception, e:
                                    print e
                                    print traceback.format_exc()

                                if operations == "Instructions" or operations == "i":

                                    if (side == "Buy"):
                                        
                                            try:
                                                worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                            except Exception, e:
                                                print e
                                                print traceback.format_exc()


                                    if (side == "Sell"):

                                            try:
                                                worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                            except Exception, e:
                                                print e
                                                print traceback.format_exc()


                                    e.mailbulkInternal(elist, "trade_deal")
                                    worksheet.update_cell(googleCell+str(numrow+1), "Emailed")

                                elif operations == "Verify":
                                    e.mailbulkExternal(elist, "trade_deal")
                                    try:
                                        worksheet.update_cell(googleCell+str(numrow+1), "Emailed")
                                    except Exception, e:
                                        print e
                                        print traceback.format_exc()      

                            else:
                                try:
                                    worksheet.update_cell(googleCell+str(numrow+1), "Rejected")

                                except Exception, e:
                                    print e
                                    print traceback.format_exc()




                else: 
                    print "Error Column="+str(numcols)

            time.sleep(30)

def main():
    # parse command line options

    sample = SimpleCRUD()
            #sample._PrintFeed()

if __name__ == '__main__':
    main()

